// 1. What directive is used by Node.js in loading the modules it needs?
	
	we use "require" directive to load Node.js modules


// 2. What Node.js module contains a method for server creation?

	let http=require("http");


// 3. What is the method of the http object responsible for creating a server using Node.js?
	
	

	http.createServer()

	-it is a method that accepts a function as its argument for creation of a server



// 4. What method of the response object allows us to set status codes and content types?
	
	response.writeHead(200,{"Content-Type":"text/plain"});

	we use writeHead to set a status or response from the server


// 5. Where will console.log() output its contents when run in Node.js?
	
	in the browser of the client/user, it will display it contents as a string.

// 6. What property of the request object contains the address's endpoint?

	response.end();

	denotes the end of the communication response from the server

